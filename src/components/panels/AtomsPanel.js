import React, {useState, useEffect, memo} from 'react';
import PropTypes from "prop-types";
import {Dropdown, Form, FormGroup, Label, Table, Input, Button, Checkbox,} from 'semantic-ui-react';

import NumberInputForm from '../forms/NumberInputForm';
import {getChemicalElementsList} from '../../api/index';
import {potentialsList, wrappedPotentialList} from '../../data/potential';
import EditableTable from '../table/EditableTable';


const AtomsPanel = (props) => {

    const [chemicalElementsList, setChemicalElementsList] = useState([]);
    const [showUploadForm, setShowUploadForm] = useState(false);
    const [atoms, setAtoms] = useState([]);
    const [tableBody, setTableBody] = useState('');
    const [coordinateTableBody, setCoordinateTableBody] = useState('');
    const [coordinateType, setCoordinateType] = useState('Direct');
    const [editable, setEditable] = useState(true);

    useEffect(() => {
        getChemicalElementsList().then(response => {
            const elementList = response.items.map(element => {
                return (
                    {
                        key: element.description,
                        text: element.code,
                        value: element
                    });
            });
            setChemicalElementsList(elementList);
        });
    }, []);

    const setAmountOfAtoms = (amountOfAtoms) => {
        let tmpArr = [];
        for (let i = 0; i < amountOfAtoms; i++) {
            tmpArr[i] = {
                name: "",
                amount: 0,
                potential: wrappedPotentialList[0],
                coordinates: [],
            };
        }
        setAtoms(tmpArr);
    };

    useEffect(() => {
        setTableBody(createTableBody());
        setCoordinateTableBody(createCoordinateTableBody());
        props.handleAtomPanelChange(prepareData());
    }, [atoms, editable, coordinateType]);

    const prepareData = () => {
        const atomTypes = [];
        const atomCoords = [];
        atoms.forEach(atom => {
            atomTypes.push({
                chemicalElementSymbol: atom.name,
                atomCount: atom.amount,
                potentialArchiveFile: atom.potential.archiveFile,
                potentialAtomFolder: atom.potential.atomFolder
            });
            for (let i = 0; i < atom.amount; i++) {
                atomCoords.push({
                    no: i,
                    chemicalElementSymbol: atom.name,
                    x1: atom.coordinates[i][0],
                    x2: atom.coordinates[i][1],
                    x3: atom.coordinates[i][2],
                    sd1: true,
                    sd2: true,
                    sd3: true
                })
            }
        });
        return {
            atomTypes,
            coordinateType,
            selectiveDynamics: editable,
            atomCoords
        };
    };

    const handleChange = (index) => (e, {name, value}) => {
        let newAtoms = atoms.slice();
        switch (name) {
            case 'atom':
                newAtoms[index] = {...newAtoms[index], name: value.code};
                setAtoms(newAtoms);
                break;
            case 'potential':
                newAtoms[index] = {...newAtoms[index], [name]: value};
                setAtoms(newAtoms);
                break;
        }
    };

    const edit = (number) => (event) => {
        const {name, value} = event.target;
        switch (name) {
            case 'amountOfDifferentAtoms':
                setAmountOfAtoms(value);
                break;
            case 'amountOfSameAtoms':
                const newAtoms = atoms.slice();
                const atomDescription = newAtoms[number];
                let coordinates = [];
                for (let i = 0; i < value; i++) {
                    coordinates.push([0, 0, 0]);
                }
                const changedAtomDescription = {...atomDescription, amount: value, coordinates};
                newAtoms[number] = changedAtomDescription;
                setAtoms(newAtoms);
                break;
        }
    };

    const createTableBody = () => {
        var body = [];

        if (atoms.length > 0) {
            for (let i = 0; i < atoms.length; i++) {
                const selectedAtomsPotentialsList = potentialsList
                    .filter(potential => atoms[i].name.includes(potential.symbolName))
                    .map(potential => {
                        return {key: potential.name, text: potential.name, value: potential}
                    });

                body.push(
                    <Table.Row>
                        <Table.Cell>
                            <Dropdown
                                selection
                                name='atom'
                                options={chemicalElementsList}
                                placeholder='Select atoms'
                                onChange={handleChange(i)}/>
                        </Table.Cell>
                        <Table.Cell>
                            <NumberInputForm label={''} parameterName={'amountOfSameAtoms'}
                                             parameterValue={atoms[i].amount} handleChange={edit(i)}/>
                        </Table.Cell>
                        <Table.Cell>
                            <Dropdown
                                selection
                                name='potential'
                                options={selectedAtomsPotentialsList}
                                disabled={selectedAtomsPotentialsList.length === 0}
                                placeholder='Select potential'
                                onChange={handleChange(i)}/>
                        </Table.Cell>
                    </Table.Row>
                )
            }
        }
        return body;
    };

    const onCoordinateChange = (enviroment) => (event, {name, value}) => {
        const {atom, atomIndex ,index} = enviroment;
        const {elIndex} = name;
        const {coordinates} = atom;
        coordinates[index].splice(elIndex, 1, value);
        const filtredAtoms = atoms.filter(el => el.name !== atom.name);
        const atomWithNewCoordinates = {...atom, coordinates};
        const newAtoms = atoms.slice();
        newAtoms.splice(atomIndex, 1, atomWithNewCoordinates);
        setAtoms(newAtoms);
    };

    const createCoordinateTableBody = () => {
        const body = [];
        let atomIndex = 1;//TODO fix table size
        atoms.forEach((atom, index) => {
            for (let i = 0; i < atom.amount; i++) {
                body.push(
                    <Table.Row>
                        <Table.Cell/>
                        <Table.Cell>{atomIndex}</Table.Cell>
                        <Table.Cell>{atom.name}</Table.Cell>
                        {
                            editable ?
                                <EditableTable data={atom.coordinates[i]}
                                               onChange={onCoordinateChange({atom, atomIndex: index, index: i})}
                                               additionalElements={selectiveDynamicsCheckBox}/> :
                                <Table.Row>
                                    <Table.Cell>{atom.coordinates[i][0]}</Table.Cell>
                                    <Table.Cell>{atom.coordinates[i][1]}</Table.Cell>
                                    <Table.Cell>{atom.coordinates[i][2]}</Table.Cell>
                                </Table.Row>
                        }
                    </Table.Row>
                );
                atomIndex++;
            }
        });
        return body;
    };

    const selectiveDynamicsCheckBox = () => <Checkbox  label='Selective dynamics'/>;

    const changeEditable = (event, {checked}) => {
        setEditable(checked);
    };

    return (
        <React.Fragment>
            <Label ribbon color='red'>Атомы</Label>
            <div style={{maxHeight: '500px', overflowX: 'auto'}}>
                <Form>
                    <FormGroup>
                        <NumberInputForm label={'Число различных атомов'} parameterName={'amountOfDifferentAtoms'}
                                         parameterValue={atoms.length}
                                         handleChange={edit()}/>
                    </FormGroup>
                </Form>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Элемент</Table.HeaderCell>
                            <Table.HeaderCell>Количество атомов</Table.HeaderCell>
                            <Table.HeaderCell>Потенциал</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {
                            tableBody
                        }
                    </Table.Body>
                </Table>

                <Button disabled onClick={() => setShowUploadForm(true)}>Загрузить координаты атомов</Button>
                {showUploadForm && <Input type='file'/>}

                <Table celled columns={5} style={{padding: '0px'}}>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>
                                <Checkbox
                                    checked={editable}
                                    onClick={changeEditable}
                                    label={''}
                                />
                            </Table.HeaderCell>
                            <Table.HeaderCell>№</Table.HeaderCell>
                            <Table.HeaderCell>Элемент</Table.HeaderCell>
                            <Table.HeaderCell colSpan='3' textAlign='center'>Координаты атомов</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {
                            coordinateTableBody
                        }
                    </Table.Body>
                </Table>
            </div>
        </React.Fragment>
    );
};

export default memo(AtomsPanel);

AtomsPanel.defaultProps = {
    handleAtomPanelChange: PropTypes.func.isRequired
};