/*
import React from 'react';
import { Button, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const ButtonComponent = (props) =>
    <Button {...props}>
    <Icon name={props.iconName}/>
        {props.label}
    </Button>;

export default ButtonComponent;


ButtonComponent.propTypes = {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

ButtonComponent.defaultProps = {
    style: {}
};*/

import React from 'react';
import {Button, Icon} from 'semantic-ui-react';
import PropTypes from 'prop-types';

const ButtonComponent = (props) => {
    const {onClickFunction, style, iconProps, buttonName} = props;

    return (
        <Button onClick={() => {
            onClickFunction();
        }
        }
                style={style}
                basic
        >
            <Icon {...iconProps}/>
            {buttonName}
        </Button>
    );
};


ButtonComponent.propTypes = {
    buttonName: PropTypes.string.isRequired,
    onClickFunction: PropTypes.func.isRequired
};

ButtonComponent.defaultProps = {
    style: {},
    iconProps: {},
};

export default ButtonComponent;