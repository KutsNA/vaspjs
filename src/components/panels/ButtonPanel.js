import React, {useState, useEffect} from 'react';
import {Button, Icon, List, ListItem, Select} from 'semantic-ui-react';

import ButtonComponent from '../ButtonComponent';
import LogPanel from './LogPanel';

import {getCalculationInfo, getUserListOfActiveTask} from '../../api/index';

const style = {
  marginBottom: '15px',
};

const ButtonPanel = props => {

  const {lastActiveRun, profiles, onProfileSelection} = props;

  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    updateTasksList();
  }, [lastActiveRun]);

  // useEffect(()=>{//add new watcher
  //     const interval = setInterval(()=>{}, 5000);
  //     return () => clearInterval(interval);
  // }, [tasks]);

  const profilesList = Object.keys(profiles).map(key => {
    return {'key': key, text: key, value: {profileName: key, ...profiles[key]}};
  });

  const onChange = (event, {name, value}) => {
    onProfileSelection(value);
  };

  const updateTasksList = () => {
    getUserListOfActiveTask().then(response => response.status === 200 ?
      setTasks(Array.isArray(response.data) ? response.data : [response.data]) :
      console.error(response)
    );
  };

  return (
    <React.Fragment>
      <Select name={'profile'} placeholder='Select profile' onChange={onChange} options={profilesList}/>
      <Button.Group vertical widths='2'>
        <ButtonComponent
          buttonName={'Запуск расчета'}
          onClickFunction={props.startCalculation}
          style={style}
          iconProps={
            {color: 'green', name: 'play'}
          }
        />
        <ButtonComponent
          buttonName={'List of user\'s task'}
          onClickFunction={updateTasksList}
          style={style}
          iconProps={
            {color: 'black', name: 'tasks'}
          }
        />
      </Button.Group>
      <List>
        <LogPanel task={{}}/>
        {/*<Button onClick={() => {
          console.log(getCalculationInfo('f47e5c81-8f4c-4c54-935a-ea44278a27fd').then(resp => resp.data.status));
        }
        }>
          Check status
        </Button>
        */}
        {
          tasks.length !== 0 && tasks.map(task =>
            <ListItem>
              <LogPanel task={task}/>
            </ListItem>
          )
        }
      </List>

    </React.Fragment>
  );
};

export default ButtonPanel;