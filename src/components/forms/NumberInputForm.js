import React from "react";
import PropTypes from "prop-types";

import FormComponent from './FormComponent'

const NumberInputForm = (props) => {
    const {label, handleChange, parameterName, parameterValue} = props;
    return (
        <FormComponent handleChange={handleChange} label={label} parameterName={parameterName} parameterValue={parameterValue}/>
    );
};

export default NumberInputForm;

NumberInputForm.propTypes = {
    label: PropTypes.string.isRequired,
    parameterName: PropTypes.string.isRequired,
    parameterValue: PropTypes.any.isRequired,
    handleChange: PropTypes.func.isRequired
};