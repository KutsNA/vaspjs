import React from 'react';
import PropTypes from "prop-types";
import EditableTable from './table/EditableTable';

const LatticeVectorsComponent = (props) => {
    const {latticeVectors, onLatticeVectorsChange} = props;
    return (
        <EditableTable data={latticeVectors} onChange={onLatticeVectorsChange}/>
    );
};

export default LatticeVectorsComponent;


LatticeVectorsComponent.propTypes = {
    latticeVectors: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
    onLatticeVectorsChange: PropTypes.func.isRequired,
};