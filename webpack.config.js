const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});

module.exports = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.jsx?$/,
                include: /node_modules/,
                use: ['react-hot-loader/webpack'],
            },
            /*{
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['eslint-loader']
            }*/
        ]
    },
    devServer: {
        proxy: {
            '/api': {
                target: 'http://crystal.pmoproject.ru',
                changeOrigin: true,
                auth: "rewnik:rewnikcrystal"
            },
            '/vasp': {
                target: 'http://crystal.pmoproject.ru',
                changeOrigin: true
            },
        }
    },
    plugins: [htmlPlugin]
};
