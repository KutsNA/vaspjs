const parser = require('fast-xml-parser');

export function parseXmlToJson(xml){
  return parser.parse(xml);
};