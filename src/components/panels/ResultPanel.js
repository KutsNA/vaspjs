import React, {useState, useEffect, memo} from 'react';
import PropTypes from "prop-types";
import ReactJson from 'react-json-view';
import {Button, Modal, Icon} from 'semantic-ui-react';

import {getResult} from '../../api/index';
import {parseXmlToJson} from '../../utils/index';

const ResultPage = (props) => {
  const {buttonStyle, task} = props;

  const [result, setResult] = useState('');

  const getXMLResult = (/*task.id*/) => {
    getResult(/*task.id*/).then(response => setResult(parseXmlToJson(response.data.vaspXml)));
  };

  return (
    <Modal trigger={
      <Button onClick={getXMLResult} style={buttonStyle}>
        <Icon name='file alternate outline'/> Show results
      </Button>
    }>
      <Modal.Header>
        {`Job ${task.id} result`}
      </Modal.Header>
      <Modal.Content image scrolling>
        <Modal.Description>
          <Modal.Header>Modal header</Modal.Header>
          <ReactJson src={result} name={false}/>
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
};

ResultPage.propTypes={

};

export default ResultPage;