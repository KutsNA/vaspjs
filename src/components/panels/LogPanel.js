import React, {useState, useEffect, memo} from 'react';
import {Button, Icon, Modal, Table} from 'semantic-ui-react';
import PropTypes from "prop-types";
import ResultPanel from './ResultPanel';
import {getResult, getCalculationInfo, getCalculationLogs, stopCalculation} from '../../api/index';

const style = {
  marginBottom: '15px',
};

const taskTerminatedStatus = ['FAILED', 'COMPLETED'];

const LogPanel = props => {
  const {task} = props;
  const [body, setBody] = useState([]);
  const [taskStatus, setTaskStatus] = useState('');

  const chooseButtonColor = () => {
    switch (taskStatus) {
      case 'FAILED':
        return 'red';
      case 'COMPLETED':
        return 'green';
      default:
        return 'grey';
    }
  };

  useEffect(() => {//add new watcher
    const interval = setInterval(getLogs, 5000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {//add new watcher
    if (task.id !== undefined) {
      const interval = setInterval(checkStatus, 5000);
      return () => clearInterval(interval);
    }
  }, []);

  const checkStatus = () => {
    if (task.id !== undefined && !taskTerminatedStatus.includes(taskStatus)) {
      getCalculationInfo(task.id).then(response => {
        //console.log(response.data.status);
        setTaskStatus(response.data.status)
      });
    }
  };

  useEffect(()=>{
    //console.log(taskStatus);
    //console.log(!taskTerminatedStatus.includes(taskStatus));
  },[taskStatus]);

  const getLogs = () => {
    if (task.id !== undefined && !taskTerminatedStatus.includes(taskStatus)) {
      getCalculationLogs(task.id).then(response => response ?
        setBody(createLogTable(response.data)) :
        console.error(response)
      );
    }
  };

  const createLogTable = data => {
    return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Seq</Table.HeaderCell>
            <Table.HeaderCell>Time</Table.HeaderCell>
            <Table.HeaderCell>Action</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {
            data.reverse().map(dataEl => (
              <Table.Row>
                <Table.Cell>{dataEl.seq}</Table.Cell>
                <Table.Cell>{dataEl.time}</Table.Cell>
                <Table.Cell>{dataEl.log}</Table.Cell>
              </Table.Row>
            ))
          }
        </Table.Body>
      </Table>
    );
  };

  return (
    <Modal trigger={<Button color={chooseButtonColor()} onClick={() => getLogs()}>{task.id}</Button>}>
      <Modal.Header>
        {`Job ${task.id} log`}
      </Modal.Header>
      <Modal.Content image scrolling>
        <Modal.Description>
          <Modal.Header>Modal header</Modal.Header>
          {
            body
          }
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          onClick={() => {
            alert('Нажата кнопка "Остановка расчета" ');
            stopCalculation(task.id);
          }}
          style={style}
          basic
        >
          <Icon color='red' name='stop'/>
          Остановка расчета
        </Button>
        <ResultPanel buttonStyle={style} task={{}}/>
      </Modal.Actions>
    </Modal>
  );
};

LogPanel.propTypes={
  task: PropTypes.shape({
    endTime: PropTypes.string,
    id: PropTypes.string,
    startTime: PropTypes.string,
    status: PropTypes.string,
    userId: PropTypes.string,
  }).isRequired
};

export default memo(LogPanel);