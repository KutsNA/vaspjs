export const potentialsList = [
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ac",
        symbolName: "Ac",
        name: "paw_gga/Ac"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ac_s",
        symbolName: "Ac",
        name: "paw_gga/Ac_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ag",
        symbolName: "Ag",
        name: "paw_gga/Ag"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Al",
        symbolName: "Al",
        name: "paw_gga/Al"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Al_h",
        symbolName: "Al",
        name: "paw_gga/Al_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ar",
        symbolName: "Ar",
        name: "paw_gga/Ar"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "As",
        symbolName: "As",
        name: "paw_gga/As"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Au",
        symbolName: "Au",
        name: "paw_gga/Au"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "B",
        symbolName: "B",
        name: "paw_gga/B"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "B_h",
        symbolName: "B",
        name: "paw_gga/B_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "B_s",
        symbolName: "B",
        name: "paw_gga/B_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ba_sv",
        symbolName: "Ba",
        name: "paw_gga/Ba_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Be",
        symbolName: "Be",
        name: "paw_gga/Be"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Be_sv",
        symbolName: "Be",
        name: "paw_gga/Be_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Bi",
        symbolName: "Bi",
        name: "paw_gga/Bi"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Bi_d",
        symbolName: "Bi",
        name: "paw_gga/Bi_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Br",
        symbolName: "Br",
        name: "paw_gga/Br"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "C",
        symbolName: "C",
        name: "paw_gga/C"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "C_h",
        symbolName: "C",
        name: "paw_gga/C_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "C_s",
        symbolName: "C",
        name: "paw_gga/C_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ca",
        symbolName: "Ca",
        name: "paw_gga/Ca"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ca_pv",
        symbolName: "Ca",
        name: "paw_gga/Ca_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ca_sv",
        symbolName: "Ca",
        name: "paw_gga/Ca_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cd",
        symbolName: "Cd",
        name: "paw_gga/Cd"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ce",
        symbolName: "Ce",
        name: "paw_gga/Ce"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ce_3",
        symbolName: "Ce",
        name: "paw_gga/Ce_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ce_s",
        symbolName: "Ce",
        name: "paw_gga/Ce_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cl",
        symbolName: "Cl",
        name: "paw_gga/Cl"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cl_h",
        symbolName: "Cl",
        name: "paw_gga/Cl_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Co",
        symbolName: "Co",
        name: "paw_gga/Co"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cr",
        symbolName: "Cr",
        name: "paw_gga/Cr"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cr_pv",
        symbolName: "Cr",
        name: "paw_gga/Cr_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cs_sv",
        symbolName: "Cs",
        name: "paw_gga/Cs_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cu",
        symbolName: "Cu",
        name: "paw_gga/Cu"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Cu_pv",
        symbolName: "Cu",
        name: "paw_gga/Cu_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Dy_3",
        symbolName: "Dy",
        name: "paw_gga/Dy_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Er_3",
        symbolName: "Er",
        name: "paw_gga/Er_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Eu_2",
        symbolName: "Eu",
        name: "paw_gga/Eu_2"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "F",
        symbolName: "F",
        name: "paw_gga/F"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "F_h",
        symbolName: "F",
        name: "paw_gga/F_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "F_s",
        symbolName: "F",
        name: "paw_gga/F_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Fe",
        symbolName: "Fe",
        name: "paw_gga/Fe"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Fe_pv",
        symbolName: "Fe",
        name: "paw_gga/Fe_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Fe_sv",
        symbolName: "Fe",
        name: "paw_gga/Fe_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ga",
        symbolName: "Ga",
        name: "paw_gga/Ga"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ga_d",
        symbolName: "Ga",
        name: "paw_gga/Ga_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ga_h",
        symbolName: "Ga",
        name: "paw_gga/Ga_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Gd_3",
        symbolName: "Gd",
        name: "paw_gga/Gd_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ge",
        symbolName: "Ge",
        name: "paw_gga/Ge"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ge_d",
        symbolName: "Ge",
        name: "paw_gga/Ge_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ge_h",
        symbolName: "Ge",
        name: "paw_gga/Ge_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "H",
        symbolName: "H",
        name: "paw_gga/H"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "H.5",
        symbolName: "H",
        name: "paw_gga/H.5"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "H.75",
        symbolName: "H",
        name: "paw_gga/H.75"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "H1.25",
        symbolName: "H",
        name: "paw_gga/H1.25"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "H1.5",
        symbolName: "H",
        name: "paw_gga/H1.5"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "H_h",
        symbolName: "H",
        name: "paw_gga/H_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "He",
        symbolName: "He",
        name: "paw_gga/He"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Hf",
        symbolName: "Hf",
        name: "paw_gga/Hf"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Hf_pv",
        symbolName: "Hf",
        name: "paw_gga/Hf_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Hg",
        symbolName: "Hg",
        name: "paw_gga/Hg"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ho_3",
        symbolName: "Ho",
        name: "paw_gga/Ho_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "I",
        symbolName: "I",
        name: "paw_gga/I"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "In",
        symbolName: "In",
        name: "paw_gga/In"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "In_d",
        symbolName: "In",
        name: "paw_gga/In_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ir",
        symbolName: "Ir",
        name: "paw_gga/Ir"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "K_pv",
        symbolName: "K",
        name: "paw_gga/K_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "K_sv",
        symbolName: "K",
        name: "paw_gga/K_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Kr",
        symbolName: "Kr",
        name: "paw_gga/Kr"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "La",
        symbolName: "La",
        name: "paw_gga/La"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "La_s",
        symbolName: "La",
        name: "paw_gga/La_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Li",
        symbolName: "Li",
        name: "paw_gga/Li"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Li_sv",
        symbolName: "Li",
        name: "paw_gga/Li_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Lu_3",
        symbolName: "Lu",
        name: "paw_gga/Lu_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Mg",
        symbolName: "Mg",
        name: "paw_gga/Mg"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Mg_pv",
        symbolName: "Mg",
        name: "paw_gga/Mg_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Mn",
        symbolName: "Mn",
        name: "paw_gga/Mn"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Mn_pv",
        symbolName: "Mn",
        name: "paw_gga/Mn_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Mo",
        symbolName: "Mo",
        name: "paw_gga/Mo"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Mo_pv",
        symbolName: "Mo",
        name: "paw_gga/Mo_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "N",
        symbolName: "N",
        name: "paw_gga/N"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "N_h",
        symbolName: "N",
        name: "paw_gga/N_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "N_s",
        symbolName: "N",
        name: "paw_gga/N_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Na",
        symbolName: "Na",
        name: "paw_gga/Na"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Na_pv",
        symbolName: "Na",
        name: "paw_gga/Na_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Na_sv",
        symbolName: "Na",
        name: "paw_gga/Na_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Nb_pv",
        symbolName: "Nb",
        name: "paw_gga/Nb_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Nb_sv",
        symbolName: "Nb",
        name: "paw_gga/Nb_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Nd_3",
        symbolName: "Nd",
        name: "paw_gga/Nd_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ne",
        symbolName: "Ne",
        name: "paw_gga/Ne"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ni",
        symbolName: "Ni",
        name: "paw_gga/Ni"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ni_pv",
        symbolName: "Ni",
        name: "paw_gga/Ni_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Np",
        symbolName: "Np",
        name: "paw_gga/Np"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Np_s",
        symbolName: "Np",
        name: "paw_gga/Np_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "O",
        symbolName: "O",
        name: "paw_gga/O"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "O_h",
        symbolName: "O",
        name: "paw_gga/O_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "O_s",
        symbolName: "O",
        name: "paw_gga/O_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Os",
        symbolName: "Os",
        name: "paw_gga/Os"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Os_pv",
        symbolName: "Os",
        name: "paw_gga/Os_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "P",
        symbolName: "P",
        name: "paw_gga/P"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "P_h",
        symbolName: "P",
        name: "paw_gga/P_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pa",
        symbolName: "Pa",
        name: "paw_gga/Pa"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pa_s",
        symbolName: "Pa",
        name: "paw_gga/Pa_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pb",
        symbolName: "Pb",
        name: "paw_gga/Pb"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pb_d",
        symbolName: "Pb",
        name: "paw_gga/Pb_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pd",
        symbolName: "Pd",
        name: "paw_gga/Pd"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pd_pv",
        symbolName: "Pd",
        name: "paw_gga/Pd_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pm_3",
        symbolName: "Pm",
        name: "paw_gga/Pm_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pr_3",
        symbolName: "Pr",
        name: "paw_gga/Pr_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pt",
        symbolName: "Pt",
        name: "paw_gga/Pt"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pu",
        symbolName: "Pu",
        name: "paw_gga/Pu"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Pu_s",
        symbolName: "Pu",
        name: "paw_gga/Pu_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Rb_pv",
        symbolName: "Rb",
        name: "paw_gga/Rb_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Rb_sv",
        symbolName: "Rb",
        name: "paw_gga/Rb_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Re",
        symbolName: "Re",
        name: "paw_gga/Re"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Re_pv",
        symbolName: "Re",
        name: "paw_gga/Re_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Rh",
        symbolName: "Rh",
        name: "paw_gga/Rh"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Rh_pv",
        symbolName: "Rh",
        name: "paw_gga/Rh_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ru",
        symbolName: "Ru",
        name: "paw_gga/Ru"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ru_pv",
        symbolName: "Ru",
        name: "paw_gga/Ru_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ru_sv",
        symbolName: "Ru",
        name: "paw_gga/Ru_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "S",
        symbolName: "S",
        name: "paw_gga/S"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "S_h",
        symbolName: "S",
        name: "paw_gga/S_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sb",
        symbolName: "Sb",
        name: "paw_gga/Sb"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sc",
        symbolName: "Sc",
        name: "paw_gga/Sc"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sc_sv",
        symbolName: "Sc",
        name: "paw_gga/Sc_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Se",
        symbolName: "Se",
        name: "paw_gga/Se"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Si",
        symbolName: "Si",
        name: "paw_gga/Si"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Si_h",
        symbolName: "Si",
        name: "paw_gga/Si_h"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sm_2",
        symbolName: "Sm",
        name: "paw_gga/Sm_2"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sm_3",
        symbolName: "Sm",
        name: "paw_gga/Sm_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sn",
        symbolName: "Sn",
        name: "paw_gga/Sn"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sn_d",
        symbolName: "Sn",
        name: "paw_gga/Sn_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Sr_sv",
        symbolName: "Sr",
        name: "paw_gga/Sr_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ta",
        symbolName: "Ta",
        name: "paw_gga/Ta"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ta_pv",
        symbolName: "Ta",
        name: "paw_gga/Ta_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tb_3",
        symbolName: "Tb",
        name: "paw_gga/Tb_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tc",
        symbolName: "Tc",
        name: "paw_gga/Tc"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tc_pv",
        symbolName: "Tc",
        name: "paw_gga/Tc_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Te",
        symbolName: "Te",
        name: "paw_gga/Te"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Th",
        symbolName: "Th",
        name: "paw_gga/Th"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Th_s",
        symbolName: "Th",
        name: "paw_gga/Th_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ti",
        symbolName: "Ti",
        name: "paw_gga/Ti"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ti_pv",
        symbolName: "Ti",
        name: "paw_gga/Ti_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Ti_sv",
        symbolName: "Ti",
        name: "paw_gga/Ti_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tl",
        symbolName: "Tl",
        name: "paw_gga/Tl"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tl_d",
        symbolName: "Tl",
        name: "paw_gga/Tl_d"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tm",
        symbolName: "Tm",
        name: "paw_gga/Tm"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Tm_3",
        symbolName: "Tm",
        name: "paw_gga/Tm_3"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "U",
        symbolName: "U",
        name: "paw_gga/U"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "U_s",
        symbolName: "U",
        name: "paw_gga/U_s"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "V",
        symbolName: "V",
        name: "paw_gga/V"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "V_pv",
        symbolName: "V",
        name: "paw_gga/V_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "V_sv",
        symbolName: "V",
        name: "paw_gga/V_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "W",
        symbolName: "W",
        name: "paw_gga/W"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "W_pv",
        symbolName: "W",
        name: "paw_gga/W_pv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Xe",
        symbolName: "Xe",
        name: "paw_gga/Xe"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Y_sv",
        symbolName: "Y",
        name: "paw_gga/Y_sv"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Yb",
        symbolName: "Yb",
        name: "paw_gga/Yb"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Yb_2",
        symbolName: "Yb",
        name: "paw_gga/Yb_2"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Zn",
        symbolName: "Zn",
        name: "paw_gga/Zn"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Zr",
        symbolName: "Zr",
        name: "paw_gga/Zr"
    },
    {
        archiveFile: "potcar_paw_gga.tar",
        atomFolder: "Zr_sv",
        symbolName: "Zr",
        name: "paw_gga/Zr_sv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ag",
        symbolName: "Ag",
        name: "us_gga/Ag"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Al",
        symbolName: "Al",
        name: "us_gga/Al"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Al_h",
        symbolName: "Al",
        name: "us_gga/Al_h"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ar",
        symbolName: "Ar",
        name: "us_gga/Ar"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "As",
        symbolName: "As",
        name: "us_gga/As"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Au",
        symbolName: "Au",
        name: "us_gga/Au"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "B",
        symbolName: "B",
        name: "us_gga/B"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "B_s",
        symbolName: "B",
        name: "us_gga/B_s"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ba_pv",
        symbolName: "Ba",
        name: "us_gga/Ba_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Be",
        symbolName: "Be",
        name: "us_gga/Be"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Bi",
        symbolName: "Bi",
        name: "us_gga/Bi"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Br",
        symbolName: "Br",
        name: "us_gga/Br"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "C",
        symbolName: "C",
        name: "us_gga/C"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "C_s",
        symbolName: "C",
        name: "us_gga/C_s"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ca",
        symbolName: "Ca",
        name: "us_gga/Ca"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ca_pv",
        symbolName: "Ca",
        name: "us_gga/Ca_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Cd",
        symbolName: "Cd",
        name: "us_gga/Cd"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Cl",
        symbolName: "Cl",
        name: "us_gga/Cl"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Co",
        symbolName: "Co",
        name: "us_gga/Co"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Cr",
        symbolName: "Cr",
        name: "us_gga/Cr"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Cs",
        symbolName: "Cs",
        name: "us_gga/Cs"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Cs_pv",
        symbolName: "Cs",
        name: "us_gga/Cs_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Cu",
        symbolName: "Cu",
        name: "us_gga/Cu"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "F",
        symbolName: "F",
        name: "us_gga/F"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "F_s",
        symbolName: "F",
        name: "us_gga/F_s"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Fe",
        symbolName: "Fe",
        name: "us_gga/Fe"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ga",
        symbolName: "Ga",
        name: "us_gga/Ga"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ga_d",
        symbolName: "Ga",
        name: "us_gga/Ga_d"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ge",
        symbolName: "Ge",
        name: "us_gga/Ge"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "H_200eV",
        symbolName: "H",
        name: "us_gga/H_200eV"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "H_soft",
        symbolName: "H",
        name: "us_gga/H_soft"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Hf",
        symbolName: "Hf",
        name: "us_gga/Hf"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Hg",
        symbolName: "Hg",
        name: "us_gga/Hg"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "I",
        symbolName: "I",
        name: "us_gga/I"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "In",
        symbolName: "In",
        name: "us_gga/In"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "In_d",
        symbolName: "In",
        name: "us_gga/In_d"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ir",
        symbolName: "Ir",
        name: "us_gga/Ir"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "K",
        symbolName: "K",
        name: "us_gga/K"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "K_pv",
        symbolName: "K",
        name: "us_gga/K_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Kr",
        symbolName: "Kr",
        name: "us_gga/Kr"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Li",
        symbolName: "Li",
        name: "us_gga/Li"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Li_h",
        symbolName: "Li",
        name: "us_gga/Li_h"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Li_pv",
        symbolName: "Li",
        name: "us_gga/Li_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Mg",
        symbolName: "Mg",
        name: "us_gga/Mg"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Mg_h",
        symbolName: "Mg",
        name: "us_gga/Mg_h"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Mg_pv",
        symbolName: "Mg",
        name: "us_gga/Mg_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Mn",
        symbolName: "Mn",
        name: "us_gga/Mn"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Mo",
        symbolName: "Mo",
        name: "us_gga/Mo"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Mo_pv",
        symbolName: "Mo",
        name: "us_gga/Mo_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "N",
        symbolName: "N",
        name: "us_gga/N"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "N_s",
        symbolName: "N",
        name: "us_gga/N_s"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Na",
        symbolName: "Na",
        name: "us_gga/Na"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Na_h",
        symbolName: "Na",
        name: "us_gga/Na_h"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Na_pv",
        symbolName: "Na",
        name: "us_gga/Na_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Nb",
        symbolName: "Nb",
        name: "us_gga/Nb"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Nb_pv",
        symbolName: "Nb",
        name: "us_gga/Nb_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ne",
        symbolName: "Ne",
        name: "us_gga/Ne"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ni",
        symbolName: "Ni",
        name: "us_gga/Ni"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "O",
        symbolName: "O",
        name: "us_gga/O"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "O_s",
        symbolName: "O",
        name: "us_gga/O_s"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Os",
        symbolName: "Os",
        name: "us_gga/Os"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "P",
        symbolName: "P",
        name: "us_gga/P"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Pb",
        symbolName: "Pb",
        name: "us_gga/Pb"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Pb_d",
        symbolName: "Pb",
        name: "us_gga/Pb_d"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Pd",
        symbolName: "Pd",
        name: "us_gga/Pd"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Pt",
        symbolName: "Pt",
        name: "us_gga/Pt"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Rb",
        symbolName: "Rb",
        name: "us_gga/Rb"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Rb_pv",
        symbolName: "Rb",
        name: "us_gga/Rb_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Re",
        symbolName: "Re",
        name: "us_gga/Re"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Rh",
        symbolName: "Rh",
        name: "us_gga/Rh"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ru",
        symbolName: "Ru",
        name: "us_gga/Ru"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "S",
        symbolName: "S",
        name: "us_gga/S"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sb",
        symbolName: "Sb",
        name: "us_gga/Sb"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sc",
        symbolName: "Sc",
        name: "us_gga/Sc"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sc_pv",
        symbolName: "Sc",
        name: "us_gga/Sc_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Se",
        symbolName: "Se",
        name: "us_gga/Se"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Si",
        symbolName: "Si",
        name: "us_gga/Si"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Si_h",
        symbolName: "Si",
        name: "us_gga/Si_h"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sn",
        symbolName: "Sn",
        name: "us_gga/Sn"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sn_d",
        symbolName: "Sn",
        name: "us_gga/Sn_d"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sr",
        symbolName: "Sr",
        name: "us_gga/Sr"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Sr_pv",
        symbolName: "Sr",
        name: "us_gga/Sr_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ta",
        symbolName: "Ta",
        name: "us_gga/Ta"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Tc",
        symbolName: "Tc",
        name: "us_gga/Tc"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Te",
        symbolName: "Te",
        name: "us_gga/Te"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ti",
        symbolName: "Ti",
        name: "us_gga/Ti"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Ti_pv",
        symbolName: "Ti",
        name: "us_gga/Ti_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Tl_d",
        symbolName: "Tl",
        name: "us_gga/Tl_d"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "V",
        symbolName: "V",
        name: "us_gga/V"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "V_pv",
        symbolName: "V",
        name: "us_gga/V_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "W",
        symbolName: "W",
        name: "us_gga/W"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Xe",
        symbolName: "Xe",
        name: "us_gga/Xe"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Y",
        symbolName: "Y",
        name: "us_gga/Y"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Y_pv",
        symbolName: "Y",
        name: "us_gga/Y_pv"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Zn",
        symbolName: "Zn",
        name: "us_gga/Zn"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Zr",
        symbolName: "Zr",
        name: "us_gga/Zr"
    },
    {
        archiveFile: "potcar_us_gga.tar",
        atomFolder: "Zr_pv",
        symbolName: "Zr",
        name: "us_gga/Zr_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ag",
        symbolName: "Ag",
        name: "us_lda/Ag"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Al",
        symbolName: "Al",
        name: "us_lda/Al"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Al_h",
        symbolName: "Al",
        name: "us_lda/Al_h"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ar",
        symbolName: "Ar",
        name: "us_lda/Ar"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "As",
        symbolName: "As",
        name: "us_lda/As"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Au",
        symbolName: "Au",
        name: "us_lda/Au"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "B",
        symbolName: "B",
        name: "us_lda/B"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "B_s",
        symbolName: "B",
        name: "us_lda/B_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ba_pv",
        symbolName: "Ba",
        name: "us_lda/Ba_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Be",
        symbolName: "Be",
        name: "us_lda/Be"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Bi",
        symbolName: "Bi",
        name: "us_lda/Bi"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Bi_d",
        symbolName: "Bi",
        name: "us_lda/Bi_d"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Br",
        symbolName: "Br",
        name: "us_lda/Br"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "C",
        symbolName: "C",
        name: "us_lda/C"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "C_s",
        symbolName: "C",
        name: "us_lda/C_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ca",
        symbolName: "Ca",
        name: "us_lda/Ca"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ca_pv",
        symbolName: "Ca",
        name: "us_lda/Ca_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Cd",
        symbolName: "Cd",
        name: "us_lda/Cd"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Cl",
        symbolName: "Cl",
        name: "us_lda/Cl"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Co",
        symbolName: "Co",
        name: "us_lda/Co"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Cr",
        symbolName: "Cr",
        name: "us_lda/Cr"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Cs",
        symbolName: "Cs",
        name: "us_lda/Cs"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Cs_pv",
        symbolName: "Cs",
        name: "us_lda/Cs_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Cu",
        symbolName: "Cu",
        name: "us_lda/Cu"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "F",
        symbolName: "F",
        name: "us_lda/F"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "F_s",
        symbolName: "F",
        name: "us_lda/F_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Fe",
        symbolName: "Fe",
        name: "us_lda/Fe"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ga",
        symbolName: "Ga",
        name: "us_lda/Ga"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ga_d",
        symbolName: "Ga",
        name: "us_lda/Ga_d"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ge",
        symbolName: "Ge",
        name: "us_lda/Ge"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "H.75",
        symbolName: "H",
        name: "us_lda/H.75"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "H1.25",
        symbolName: "H",
        name: "us_lda/H1.25"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "H_200eV",
        symbolName: "H",
        name: "us_lda/H_200eV"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "H_soft",
        symbolName: "H",
        name: "us_lda/H_soft"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Hf",
        symbolName: "Hf",
        name: "us_lda/Hf"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Hg",
        symbolName: "Hg",
        name: "us_lda/Hg"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "I",
        symbolName: "I",
        name: "us_lda/I"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "In",
        symbolName: "In",
        name: "us_lda/In"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "In_d",
        symbolName: "In",
        name: "us_lda/In_d"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ir",
        symbolName: "Ir",
        name: "us_lda/Ir"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "K",
        symbolName: "K",
        name: "us_lda/K"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "K_pv",
        symbolName: "K",
        name: "us_lda/K_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "K_s",
        symbolName: "K",
        name: "us_lda/K_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Kr",
        symbolName: "Kr",
        name: "us_lda/Kr"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Li",
        symbolName: "Li",
        name: "us_lda/Li"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Li_h",
        symbolName: "Li",
        name: "us_lda/Li_h"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Li_pv",
        symbolName: "Li",
        name: "us_lda/Li_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mg",
        symbolName: "Mg",
        name: "us_lda/Mg"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mg_h",
        symbolName: "Mg",
        name: "us_lda/Mg_h"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mg_pv",
        symbolName: "Mg",
        name: "us_lda/Mg_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mn",
        symbolName: "Mn",
        name: "us_lda/Mn"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mn_pv",
        symbolName: "Mn",
        name: "us_lda/Mn_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mo",
        symbolName: "Mo",
        name: "us_lda/Mo"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Mo_pv",
        symbolName: "Mo",
        name: "us_lda/Mo_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "N",
        symbolName: "N",
        name: "us_lda/N"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "N_s",
        symbolName: "N",
        name: "us_lda/N_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Na",
        symbolName: "Na",
        name: "us_lda/Na"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Na_h",
        symbolName: "Na",
        name: "us_lda/Na_h"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Na_pv",
        symbolName: "Na",
        name: "us_lda/Na_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Nb",
        symbolName: "Nb",
        name: "us_lda/Nb"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Nb_pv",
        symbolName: "Nb",
        name: "us_lda/Nb_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ne",
        symbolName: "Ne",
        name: "us_lda/Ne"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ni",
        symbolName: "Ni",
        name: "us_lda/Ni"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "O",
        symbolName: "O",
        name: "us_lda/O"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "O_s",
        symbolName: "O",
        name: "us_lda/O_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Os",
        symbolName: "Os",
        name: "us_lda/Os"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "P",
        symbolName: "P",
        name: "us_lda/P"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Pb",
        symbolName: "Pb",
        name: "us_lda/Pb"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Pb_d",
        symbolName: "Pb",
        name: "us_lda/Pb_d"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Pd",
        symbolName: "Pd",
        name: "us_lda/Pd"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Pt",
        symbolName: "Pt",
        name: "us_lda/Pt"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Rb",
        symbolName: "Rb",
        name: "us_lda/Rb"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Rb_pv",
        symbolName: "Rb",
        name: "us_lda/Rb_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Rb_s",
        symbolName: "Rb",
        name: "us_lda/Rb_s"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Re",
        symbolName: "Re",
        name: "us_lda/Re"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Rh",
        symbolName: "Rh",
        name: "us_lda/Rh"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ru",
        symbolName: "Ru",
        name: "us_lda/Ru"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "S",
        symbolName: "S",
        name: "us_lda/S"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sb",
        symbolName: "Sb",
        name: "us_lda/Sb"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sc",
        symbolName: "Sc",
        name: "us_lda/Sc"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sc_pv",
        symbolName: "Sc",
        name: "us_lda/Sc_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Se",
        symbolName: "Se",
        name: "us_lda/Se"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Si",
        symbolName: "Si",
        name: "us_lda/Si"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Si_h",
        symbolName: "Si",
        name: "us_lda/Si_h"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sn",
        symbolName: "Sn",
        name: "us_lda/Sn"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sn_d",
        symbolName: "Sn",
        name: "us_lda/Sn_d"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sr",
        symbolName: "Sr",
        name: "us_lda/Sr"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Sr_pv",
        symbolName: "Sr",
        name: "us_lda/Sr_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Tc",
        symbolName: "Tc",
        name: "us_lda/Tc"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Te",
        symbolName: "Te",
        name: "us_lda/Te"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ti",
        symbolName: "Ti",
        name: "us_lda/Ti"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Ti_pv",
        symbolName: "Ti",
        name: "us_lda/Ti_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Tl_d",
        symbolName: "Tl",
        name: "us_lda/Tl_d"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "V",
        symbolName: "V",
        name: "us_lda/V"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "V_pv",
        symbolName: "V",
        name: "us_lda/V_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "W",
        symbolName: "W",
        name: "us_lda/W"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Xe",
        symbolName: "Xe",
        name: "us_lda/Xe"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Y",
        symbolName: "Y",
        name: "us_lda/Y"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Y_pv",
        symbolName: "Y",
        name: "us_lda/Y_pv"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Zn",
        symbolName: "Zn",
        name: "us_lda/Zn"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Zr",
        symbolName: "Zr",
        name: "us_lda/Zr"
    },
    {
        archiveFile: "potcar_us_lda.tar",
        atomFolder: "Zr_pv",
        symbolName: "Zr",
        name: "us_lda/Zr_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ac",
        symbolName: "Ac",
        name: "paw_lda/Ac"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ac_s",
        symbolName: "Ac",
        name: "paw_lda/Ac_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ag",
        symbolName: "Ag",
        name: "paw_lda/Ag"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Al",
        symbolName: "Al",
        name: "paw_lda/Al"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Al_h",
        symbolName: "Al",
        name: "paw_lda/Al_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ar",
        symbolName: "Ar",
        name: "paw_lda/Ar"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "As",
        symbolName: "As",
        name: "paw_lda/As"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Au",
        symbolName: "Au",
        name: "paw_lda/Au"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "B",
        symbolName: "B",
        name: "paw_lda/B"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "B_h",
        symbolName: "B",
        name: "paw_lda/B_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "B_s",
        symbolName: "B",
        name: "paw_lda/B_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ba_sv",
        symbolName: "Ba",
        name: "paw_lda/Ba_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Be",
        symbolName: "Be",
        name: "paw_lda/Be"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Be_sv",
        symbolName: "Be",
        name: "paw_lda/Be_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Bi",
        symbolName: "Bi",
        name: "paw_lda/Bi"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Bi_d",
        symbolName: "Bi",
        name: "paw_lda/Bi_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Br",
        symbolName: "Br",
        name: "paw_lda/Br"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "C",
        symbolName: "C",
        name: "paw_lda/C"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "C_h",
        symbolName: "C",
        name: "paw_lda/C_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "C_s",
        symbolName: "C",
        name: "paw_lda/C_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ca_pv",
        symbolName: "Ca",
        name: "paw_lda/Ca_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ca_sv",
        symbolName: "Ca",
        name: "paw_lda/Ca_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cd",
        symbolName: "Cd",
        name: "paw_lda/Cd"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ce",
        symbolName: "Ce",
        name: "paw_lda/Ce"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ce_s",
        symbolName: "Ce",
        name: "paw_lda/Ce_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cl",
        symbolName: "Cl",
        name: "paw_lda/Cl"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cl_h",
        symbolName: "Cl",
        name: "paw_lda/Cl_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Co",
        symbolName: "Co",
        name: "paw_lda/Co"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cr",
        symbolName: "Cr",
        name: "paw_lda/Cr"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cr_pv",
        symbolName: "Cr",
        name: "paw_lda/Cr_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cs_sv",
        symbolName: "Cs",
        name: "paw_lda/Cs_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cu",
        symbolName: "Cu",
        name: "paw_lda/Cu"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Cu_pv",
        symbolName: "Cu",
        name: "paw_lda/Cu_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "F",
        symbolName: "F",
        name: "paw_lda/F"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "F_h",
        symbolName: "F",
        name: "paw_lda/F_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "F_s",
        symbolName: "F",
        name: "paw_lda/F_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Fe",
        symbolName: "Fe",
        name: "paw_lda/Fe"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Fe_pv",
        symbolName: "Fe",
        name: "paw_lda/Fe_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ga",
        symbolName: "Ga",
        name: "paw_lda/Ga"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ga_d",
        symbolName: "Ga",
        name: "paw_lda/Ga_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ga_h",
        symbolName: "Ga",
        name: "paw_lda/Ga_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ge",
        symbolName: "Ge",
        name: "paw_lda/Ge"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ge_d",
        symbolName: "Ge",
        name: "paw_lda/Ge_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ge_h",
        symbolName: "Ge",
        name: "paw_lda/Ge_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "H",
        symbolName: "H",
        name: "paw_lda/H"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "H.5",
        symbolName: "H",
        name: "paw_lda/H.5"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "H.75",
        symbolName: "H",
        name: "paw_lda/H.75"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "H1.25",
        symbolName: "H",
        name: "paw_lda/H1.25"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "H1.5",
        symbolName: "H",
        name: "paw_lda/H1.5"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "H_h",
        symbolName: "H",
        name: "paw_lda/H_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "He",
        symbolName: "He",
        name: "paw_lda/He"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Hf",
        symbolName: "Hf",
        name: "paw_lda/Hf"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Hf_pv",
        symbolName: "Hf",
        name: "paw_lda/Hf_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Hg",
        symbolName: "Hg",
        name: "paw_lda/Hg"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "I",
        symbolName: "I",
        name: "paw_lda/I"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "In",
        symbolName: "In",
        name: "paw_lda/In"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "In_d",
        symbolName: "In",
        name: "paw_lda/In_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ir",
        symbolName: "Ir",
        name: "paw_lda/Ir"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "K_pv",
        symbolName: "K",
        name: "paw_lda/K_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "K_sv",
        symbolName: "K",
        name: "paw_lda/K_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Kr",
        symbolName: "Kr",
        name: "paw_lda/Kr"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "La",
        symbolName: "La",
        name: "paw_lda/La"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "La_s",
        symbolName: "La",
        name: "paw_lda/La_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Li",
        symbolName: "Li",
        name: "paw_lda/Li"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Li_sv",
        symbolName: "Li",
        name: "paw_lda/Li_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mg",
        symbolName: "Mg",
        name: "paw_lda/Mg"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mg_pv",
        symbolName: "Mg",
        name: "paw_lda/Mg_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mn",
        symbolName: "Mn",
        name: "paw_lda/Mn"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mn_pv",
        symbolName: "Mn",
        name: "paw_lda/Mn_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mo",
        symbolName: "Mo",
        name: "paw_lda/Mo"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mo_pv",
        symbolName: "Mo",
        name: "paw_lda/Mo_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Mo_sv",
        symbolName: "Mo",
        name: "paw_lda/Mo_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "N",
        symbolName: "N",
        name: "paw_lda/N"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "N_h",
        symbolName: "N",
        name: "paw_lda/N_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "N_s",
        symbolName: "N",
        name: "paw_lda/N_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Na",
        symbolName: "Na",
        name: "paw_lda/Na"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Na_pv",
        symbolName: "Na",
        name: "paw_lda/Na_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Na_sv",
        symbolName: "Na",
        name: "paw_lda/Na_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Nb_pv",
        symbolName: "Nb",
        name: "paw_lda/Nb_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Nb_sv",
        symbolName: "Nb",
        name: "paw_lda/Nb_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ne",
        symbolName: "Ne",
        name: "paw_lda/Ne"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ni",
        symbolName: "Ni",
        name: "paw_lda/Ni"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ni_pv",
        symbolName: "Ni",
        name: "paw_lda/Ni_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Np",
        symbolName: "Np",
        name: "paw_lda/Np"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Np_s",
        symbolName: "Np",
        name: "paw_lda/Np_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "O",
        symbolName: "O",
        name: "paw_lda/O"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "O_h",
        symbolName: "O",
        name: "paw_lda/O_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "O_s",
        symbolName: "O",
        name: "paw_lda/O_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Os",
        symbolName: "Os",
        name: "paw_lda/Os"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Os_pv",
        symbolName: "Os",
        name: "paw_lda/Os_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "P",
        symbolName: "P",
        name: "paw_lda/P"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "P_h",
        symbolName: "P",
        name: "paw_lda/P_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pa",
        symbolName: "Pa",
        name: "paw_lda/Pa"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pa_s",
        symbolName: "Pa",
        name: "paw_lda/Pa_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pb",
        symbolName: "Pb",
        name: "paw_lda/Pb"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pb_d",
        symbolName: "Pb",
        name: "paw_lda/Pb_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pd",
        symbolName: "Pd",
        name: "paw_lda/Pd"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pd_pv",
        symbolName: "Pd",
        name: "paw_lda/Pd_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pt",
        symbolName: "Pt",
        name: "paw_lda/Pt"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pu",
        symbolName: "Pu",
        name: "paw_lda/Pu"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Pu_s",
        symbolName: "Pu",
        name: "paw_lda/Pu_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Rb_pv",
        symbolName: "Rb",
        name: "paw_lda/Rb_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Rb_sv",
        symbolName: "Rb",
        name: "paw_lda/Rb_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Re",
        symbolName: "Re",
        name: "paw_lda/Re"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Re_pv",
        symbolName: "Re",
        name: "paw_lda/Re_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Rh",
        symbolName: "Rh",
        name: "paw_lda/Rh"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Rh_pv",
        symbolName: "Rh",
        name: "paw_lda/Rh_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ru",
        symbolName: "Ru",
        name: "paw_lda/Ru"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ru_pv",
        symbolName: "Ru",
        name: "paw_lda/Ru_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "S",
        symbolName: "S",
        name: "paw_lda/S"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "S_h",
        symbolName: "S",
        name: "paw_lda/S_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Sb",
        symbolName: "Sb",
        name: "paw_lda/Sb"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Sc_sv",
        symbolName: "Sc",
        name: "paw_lda/Sc_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Se",
        symbolName: "Se",
        name: "paw_lda/Se"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Si",
        symbolName: "Si",
        name: "paw_lda/Si"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Si_h",
        symbolName: "Si",
        name: "paw_lda/Si_h"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Sn",
        symbolName: "Sn",
        name: "paw_lda/Sn"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Sn_d",
        symbolName: "Sn",
        name: "paw_lda/Sn_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Sr_sv",
        symbolName: "Sr",
        name: "paw_lda/Sr_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ta",
        symbolName: "Ta",
        name: "paw_lda/Ta"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ta_pv",
        symbolName: "Ta",
        name: "paw_lda/Ta_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Tc",
        symbolName: "Tc",
        name: "paw_lda/Tc"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Tc_pv",
        symbolName: "Tc",
        name: "paw_lda/Tc_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Te",
        symbolName: "Te",
        name: "paw_lda/Te"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Th",
        symbolName: "Th",
        name: "paw_lda/Th"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Th_s",
        symbolName: "Th",
        name: "paw_lda/Th_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ti",
        symbolName: "Ti",
        name: "paw_lda/Ti"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ti_pv",
        symbolName: "Ti",
        name: "paw_lda/Ti_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Ti_sv",
        symbolName: "Ti",
        name: "paw_lda/Ti_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Tl",
        symbolName: "Tl",
        name: "paw_lda/Tl"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Tl_d",
        symbolName: "Tl",
        name: "paw_lda/Tl_d"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "U",
        symbolName: "U",
        name: "paw_lda/U"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "U_s",
        symbolName: "U",
        name: "paw_lda/U_s"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "V",
        symbolName: "V",
        name: "paw_lda/V"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "V_pv",
        symbolName: "V",
        name: "paw_lda/V_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "V_sv",
        symbolName: "V",
        name: "paw_lda/V_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "W",
        symbolName: "W",
        name: "paw_lda/W"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "W_pv",
        symbolName: "W",
        name: "paw_lda/W_pv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Xe",
        symbolName: "Xe",
        name: "paw_lda/Xe"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Y_sv",
        symbolName: "Y",
        name: "paw_lda/Y_sv"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Zn",
        symbolName: "Zn",
        name: "paw_lda/Zn"
    },
    {
        archiveFile: "potcar_paw_lda.tar",
        atomFolder: "Zr_sv",
        symbolName: "Zr",
        name: "paw_lda/Zr_sv"
    }
]

export const wrappedPotentialList = potentialsList.map(potential => {return {key: potential.name, text: potential.name, value: potential}});
