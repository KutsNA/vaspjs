import React from 'react';

import MainPage from './components/panels/MainPage';
import DataContext from './context/dataContext';
import {getDefaultConfig, getUserListOfActiveTask, initialCalculation} from './api/index';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      atoms: {
        atomTypes: [
          {
            'chemicalElementSymbol': 'Al',
            'atomCount': 4,
            'potentialArchiveFile': 'potcar_paw_gga.tar',
            'potentialAtomFolder': 'Al'
          }
        ],
        coordinateType: 'Direct',
        selectiveDynamics: true,
        atomCoords: [
          {
            'no': 1,
            'chemicalElementSymbol': 'Al',
            'x1': 0.0,
            'x2': 0.0,
            'x3': 0.0,
            'sd1': true,
            'sd2': true,
            'sd3': true
          },
          {
            'no': 2,
            'chemicalElementSymbol': 'Al',
            'x1': 0.0,
            'x2': 0.5,
            'x3': 0.5,
            'sd1': true,
            'sd2': true,
            'sd3': true
          },
          {
            'no': 3,
            'chemicalElementSymbol': 'Al',
            'x1': 0.5,
            'x2': 0.0,
            'x3': 0.5,
            'sd1': true,
            'sd2': true,
            'sd3': true
          },
          {
            'no': 4,
            'chemicalElementSymbol': 'Al',
            'x1': 0.5,
            'x2': 0.5,
            'x3': 0.0,
            'sd1': true,
            'sd2': true,
            'sd3': true
          }
        ]
      },
      incar: [
        {'code': 'SYSTEM', 'value': 'System name'},
        {'code': 'NWRITE', 'value': '2'},
        {'code': 'PREC', 'value': 'High'},
        {'code': 'NELM', 'value': '30'},
        {'code': 'NELMIN', 'value': '7'},
        {'code': 'NELMDL', 'value': '-6'},
        {'code': 'EDIFF', 'value': '1.0e-4'},
        {'code': 'ISMEAR', 'value': '2'},
        {'code': 'SIGMA', 'value': '0.2'},
        {'code': 'VOSKOWN', 'value': 'true'},
        {'code': 'ISPIN', 'value': '2'},
        {'code': 'LWAVE', 'value': 'false'},
        {'code': 'LCHARG', 'value': 'true'},
        {'code': 'NEDOS', 'value': '2500'},
        {'code': 'NSW', 'value': '50'},
        {'code': 'EDIFFG', 'value': '1.0E-2'},
        {'code': 'IBRION', 'value': '1'},
        {'code': 'ISIF', 'value': '3'}
      ],
      kPoints: {
        meshGenerationType: 'Gamma',
        length: 0,
        divisions: [
          11,
          11,
          11
        ],
        shifts: [
          0.0,
          0.0,
          0.0
        ],
        basisVectors: [
          [
            1.0,
            0.0,
            0.0
          ],
          [
            0.0,
            1.0,
            0.0
          ],
          [
            0.0,
            0.0,
            1.0
          ]
        ]
      },
      parameters: [],
      profiles: [],
      selectedProfile: 'MAI_SingleNode',
      structure: {
        latticeConstant: 1.0,
        latticeVectors: [
          [
            3.65,
            0.0,
            0.0
          ],
          [
            0.0,
            3.65,
            0.0
          ],
          [
            0.0,
            0.0,
            3.65
          ]
        ]
      },
      activeRuns: [],
      errorMessage: '',
      lastActiveRun: {}
    };
  }

  componentDidMount() {
    getDefaultConfig().then(response => response ?
      this.setState({
          parameters: response.incarAttributeTypes,
          profiles: response.runProfiles
        }
      ) :
      this.defineErrorMessage(response)
    );
  }

  defineErrorMessage = (message) => {
    this.setState({errorMessage: message});
  };

  getUserTasks = () => {
    getUserListOfActiveTask().then(response => response ?
      this.setState({activeRuns: response.data}, () => console.log(this.state)) :
      this.defineErrorMessage(response)
    );
  };

  handleFormChange = (event, {name, value}) => {
    Object.keys(this.state).map(key => {
      if (name in this.state[key]) {
        this.setState({
          [key]: {...this.state[key], [name]: value}
        });
      }
    });
  };

  handleAtomPanelChange = (atoms) => {
    this.setState({atoms});
  };

  handleLatticeVectorsChange = (event, {name, value}) => {
    const {latticeVectors} = this.state.structure;
    const {rowIndex, elIndex} = name;
    latticeVectors[rowIndex][elIndex] = value;
    this.setState({structure: {...this.state.structure, latticeVectors}});
  };

  handleCalculationGridChange = (kPoints) => {
    this.setState(kPoints);
  };

  handleParameterPanelChange = (event, {name, value}) => {
    const parameters = this.state.parameters.slice();
    const fieldToChange = parameters.filter(parameter => parameter.code === name).pop();
    const changedField = {...fieldToChange, defaultValue: value};
    const index = parameters.indexOf(fieldToChange);
    parameters.splice(index, 1, changedField);
    this.setState({parameters});
  };

  onProfileSelection = (profile) => {
    this.setState({selectedProfile: profile});
  };

  startCalculation = () => {
    const {atoms, kPoints, parameters, structure, selectedProfile} = this.state;
    initialCalculation({atoms, kPoints, parameters, structure, selectedProfile})
      .then(response => this.setState({lastActiveRun: response.data}, () => this.getUserTasks()));
  };

  render() {
    const {atoms, activeRuns, kPoints, lastActiveRun, parameters, profiles, structure} = this.state;
    return (
      <DataContext.Provider value={{
        atoms: atoms,
        activeRuns: activeRuns,
        kPoints: kPoints,
        lastActiveRun: lastActiveRun,
        parameters: parameters,
        structure: structure,
        profiles: profiles,
        getUserTasks: this.getUserTasks,
        handleFormChange: this.handleFormChange,
        handleLatticeVectorsChange: this.handleLatticeVectorsChange,
        handleAtomPanelChange: this.handleAtomPanelChange,
        handleCalculationGridChange: this.handleCalculationGridChange,
        handleParameterPanelChange: this.handleParameterPanelChange,
        onProfileSelection: this.onProfileSelection,
        startCalculation: this.startCalculation
      }}
      >
        <MainPage />
      </DataContext.Provider>
    );
  }
}

export default App;