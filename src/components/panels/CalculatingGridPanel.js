import React from 'react';
import {Form, FormGroup, Grid, GridRow, GridColumn, Label, Select, Table, Tab} from 'semantic-ui-react';
import PropTypes from "prop-types";

import NumberInputForm from '../forms/NumberInputForm';
import EditableTable from '../table/EditableTable';

export default class CalculatingGridPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props.kPoints
        };
    };

    options = [
        {key: 0, text: 'Авто', value: 'Auto'},
        {key: 1, text: 'Гамма', value: 'Gamma'},
        {key: 2, text: 'Декартова', value: 'Dekart'}
    ];

    onChange = (event, {name, value}) => this.setState(
        {[name]: value},
        () => this.props.handleCalculationGridChange(this.state)
    );
    onGridTypeChange = (event, {value}) => this.setState({meshGenerationType: value});

    onRowChange = (rowName) => (event, {name, value}) => {
        const {elIndex} = name;
        switch (rowName) {
            case 'divisor':
                let {divisions} = this.state || [0, 0, 0];
                divisions[elIndex] = value;
                this.setState({divisions});
                break;
            case 'shifts':
                let {shifts} = this.state || [0, 0, 0];
                shifts[elIndex] = value;
                this.setState({shifts});
                break;
        }
    };

    onBasisVectorsChange = (event, {name, value}) => {
        const {basisVectors} = this.state;
        const {rowIndex, elIndex} = name;
        basisVectors[rowIndex][elIndex] = value;
        this.setState({basisVectors});
    };

    render() {
        const {meshGenerationType} = this.state;
        return (
            <React.Fragment>
                <Label ribbon color='red'>Рассчетная сетка</Label>
                <Grid columns={1}>
                    <GridRow>
                        <GridColumn>
                            <Form>
                                <Form.Field>
                                    <label>Тип решетки</label>
                                    <Select options={this.options}
                                            placeholder={this.options.filter(option => option.value === meshGenerationType).shift().text}
                                            onChange={this.onGridTypeChange}/>
                                </Form.Field>
                            </Form>
                        </GridColumn>
                    </GridRow>
                    <GridRow>
                        <GridColumn>
                            {
                                meshGenerationType === 'Auto' &&
                                <FormGroup>
                                    <NumberInputForm label={"Длина"} parameterName={'length'}
                                                     parameterValue={this.state.length}
                                                     handleChange={this.onChange}/>
                                </FormGroup>
                            }
                            {
                                meshGenerationType === 'Gamma' &&
                                <Table>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell>Делители</Table.Cell>
                                            <EditableTable data={this.state.divisions}
                                                           onChange={this.onRowChange('divisor')}/>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Сдвиг</Table.Cell>
                                            <EditableTable data={this.state.shifts}
                                                           onChange={this.onRowChange('shifts')}/>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            }
                            {
                                meshGenerationType === 'Dekart' &&
                                <Table>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell>Базис</Table.Cell>
                                            <Table.Cell>
                                                <EditableTable
                                                    data={this.state.basisVectors || [[0, 0, 0], [0, 0, 0], [0, 0, 0]]}
                                                    onChange={this.onBasisVectorsChange}/>
                                            </Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Сдвиг</Table.Cell>
                                            <Table.Cell>
                                                <EditableTable data={this.state.shifts}
                                                               onChange={this.onRowChange('shifts')}/>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            }
                        </GridColumn>
                    </GridRow>
                </Grid>
            </React.Fragment>
        );
    }
}

CalculatingGridPanel.propTypes = {
    kPoints: PropTypes.object.isRequired,
    handleCalculationGridChange: PropTypes.func.isRequired,
};