import React from "react";
import {Form, FormGroup, Label} from 'semantic-ui-react';
import PropTypes from "prop-types";
import NumberInputForm from '../forms/NumberInputForm';
import LatticeVectorsComponent from '../LatticeVectorsComponent';


const GeometricPanel = (props) => {
    const {latticeConstant, latticeVectors, handleChange, handleLatticeVectorsChange} = props;
    return (
        <Form>
            <Label ribbon color='red'>Геометрия кристаллической решетки</Label>
            <FormGroup>
                <NumberInputForm label={"Постоянная решетки"}
                                 parameterName={'latticeConstant'}
                                 parameterValue={latticeConstant}
                                 handleChange={handleChange}/>
            </FormGroup>
            <LatticeVectorsComponent onLatticeVectorsChange={handleLatticeVectorsChange}
                                     latticeVectors={latticeVectors}/>
        </Form>
    );
};

export default GeometricPanel;


GeometricPanel.propTypes = {
    latticeConstant: PropTypes.number.isRequired,
    latticeVectors: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
    handleChange: PropTypes.func.isRequired,
    handleLatticeVectorsChange: PropTypes.func.isRequired,
};