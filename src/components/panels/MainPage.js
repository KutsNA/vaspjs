import React, {memo} from "react";
import {Table} from "semantic-ui-react";
import "@babel/polyfill";

import AtomsPanel from './AtomsPanel';
import ButtonPanel from './ButtonPanel';
import CalculatingGridPanel from './CalculatingGridPanel';
import GeometricPanel from './GeometricPanel';
import ParameterPanel from './ParameterPanel';

import DataContext from '../../context/dataContext';

const MainPage = () => {
    return (
        <DataContext.Consumer>
            {context => (
                <Table celled>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell width={5} verticalAlign='top'>
                                <GeometricPanel
                                    handleChange={context.handleFormChange}
                                    handleLatticeVectorsChange={context.handleLatticeVectorsChange}
                                    latticeConstant={Number.parseFloat(context.structure.latticeConstant)}
                                    latticeVectors={context.structure.latticeVectors}
                                />
                            </Table.Cell>
                            <Table.Cell width={5} verticalAlign='top'>
                                <CalculatingGridPanel kPoints={context.kPoints}
                                                      handleCalculationGridChange={context.handleCalculationGridChange}/>
                            </Table.Cell>
                            <Table.Cell width={1}>
                               <ButtonPanel lastActiveRun={context.lastActiveRun} profiles={context.profiles} onProfileSelection={context.onProfileSelection} activeRuns={context.activeRuns} startCalculation={context.startCalculation}/>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell verticalAlign='top'>
                                <AtomsPanel handleAtomPanelChange={context.handleAtomPanelChange}/>
                            </Table.Cell>
                            <Table.Cell>
                                <ParameterPanel parameters={context.parameters} handleParameterPanelChange={context.handleParameterPanelChange}/>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            )}
        </DataContext.Consumer>
    );
};

export default memo(MainPage);