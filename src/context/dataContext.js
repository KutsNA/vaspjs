import React from "react";

export default React.createContext({
    parameters: [
        {
            code: "SYSTEM",
            name: "Название",
            description: null,
            type: "string",
            defaultValue: "System name",
            options: null,
            docUrl: "http://cms.mpi.univie.ac.at/vasp/vasp/SYSTEM_tag.html"
        },
        {
            code: "NWRITE",
            name: "Уровень вывода (verbosity)",
            description: null,
            type: "int",
            defaultValue: "2",
            options: null,
            docUrl: "http://cms.mpi.univie.ac.at/vasp/vasp/NWRITE_tag.html"
        },
        {
            code: "PREC",
            name: "Точность",
            description: null,
            type: "enum",
            defaultValue: "High",
            options: [{code: "High", displayName: "Высокая"}],
            docUrl: null
        },
        {
            code: "NELM",
            name: "Макс электр SC шагов",
            description: null,
            type: "int",
            defaultValue: "30",
            options: null,
            docUrl: null
        },
        {
            code: "NELMIN",
            name: "Мин электр SC шагов",
            description: null,
            type: "int",
            defaultValue: "7",
            options: null,
            docUrl: null
        },
        {
            code: "NELMDL",
            name: "Кол-во не электр SC-шагов",
            description: null,
            type: "int",
            defaultValue: "-6",
            options: null,
            docUrl: null
        },
        {
            code: "EDIFF",
            name: "Условие выхода из электр SC-цикла",
            description: null,
            type: "double",
            defaultValue: "1e-4",
            options: null,
            docUrl: null
        },
        {
            code: "ISMEAR",
            name: "Тип размытия",
            description: null,
            type: "int",
            defaultValue: "2",
            options: null,
            docUrl: null
        },
        {
            code: "SIGMA",
            name: "Ширина размытия",
            description: null,
            type: "double",
            defaultValue: "0.2",
            options: null,
            docUrl: null
        },
        {
            code: "VOSKOWN",
            name: "Интерполяция Vosco-Wilk-Nusair",
            description: null,
            type: "bool",
            defaultValue: "true",
            options: null,
            docUrl: "http://cms.mpi.univie.ac.at/vasp/vasp/VOSKOWN_tag.html"
        },
        {
            code: "ISPIN",
            name: "Спин-поляризация",
            description: null,
            type: "int",
            defaultValue: "2",
            options: null,
            docUrl: null
        },
        {
            code: "LWAVE",
            name: "Расчет орбиталей",
            description: null,
            type: "bool",
            defaultValue: "false",
            options: null,
            docUrl: null
        },
        {
            code: "LCHARG",
            name: "Расчет зарядов",
            description: null,
            type: "bool",
            defaultValue: "true",
            options: null,
            docUrl: null
        },
        {
            code: "NEDOS",
            name: "Кол-во точек в сетке DOS",
            description: null,
            type: "int",
            defaultValue: "2500",
            options: null,
            docUrl: null
        },
        {
            code: "NSW",
            name: "Макс ионных шагов",
            description: null,
            type: "int",
            defaultValue: "50",
            options: null,
            docUrl: null
        },
        {
            code: "EDIFFG",
            name: "Условие выхода из цикла ионной релакс",
            description: null,
            type: "double",
            defaultValue: "1.0E-2",
            options: null,
            docUrl: null
        },
        {
            code: "IBRION",
            name: "Способ перемещения ионов",
            description: null,
            type: "int",
            defaultValue: "1",
            options: null,
            docUrl: null
        },
        {
            code: "ISIF",
            name: "Расчет тензора напряжений",
            description: null,
            type: "int",
            defaultValue: "3",
            options: null,
            docUrl: null
        }
    ],
    kPoints: {
        meshGenerationType: 'Gamma',
        length: 0,
        divisions: [
            11,
            11,
            11
        ],
        shifts: [
            0.0,
            0.0,
            0.0
        ],
        basisVectors: [
            [
                1.0,
                0.0,
                0.0
            ],
            [
                0.0,
                1.0,
                0.0
            ],
            [
                0.0,
                0.0,
                1.0
            ]
        ]
    },
    atoms: {
        atomTypes: [
            {
                "chemicalElementSymbol": "Al",
                "atomCount": 4,
                "potentialArchiveFile": "potcar_paw_gga.tar",
                "potentialAtomFolder": "Al"
            }
        ],
        coordinateType: 'Direct',
        selectiveDynamics: true,
        atomCoords: [
            {
                "no": 1,
                "chemicalElementSymbol": "Al",
                "x1": 0.0,
                "x2": 0.0,
                "x3": 0.0,
                "sd1": true,
                "sd2": true,
                "sd3": true
            },
            {
                "no": 2,
                "chemicalElementSymbol": "Al",
                "x1": 0.0,
                "x2": 0.5,
                "x3": 0.5,
                "sd1": true,
                "sd2": true,
                "sd3": true
            },
            {
                "no": 3,
                "chemicalElementSymbol": "Al",
                "x1": 0.5,
                "x2": 0.0,
                "x3": 0.5,
                "sd1": true,
                "sd2": true,
                "sd3": true
            },
            {
                "no": 4,
                "chemicalElementSymbol": "Al",
                "x1": 0.5,
                "x2": 0.5,
                "x3": 0.0,
                "sd1": true,
                "sd2": true,
                "sd3": true
            }
        ]
    },
    structure: {
        latticeConstant: 1.0,
        latticeVectors:  [
            [
                3.65,
                0.0,
                0.0
            ],
            [
                0.0,
                3.65,
                0.0
            ],
            [
                0.0,
                0.0,
                3.65
            ]
        ]
    },
    incar: [
        {"code": "SYSTEM", "value": "System name"},
        {"code": "NWRITE", "value": "2"},
        {"code": "PREC", "value": "High"},
        {"code": "NELM", "value": "30"},
        {"code": "NELMIN", "value": "7"},
        {"code": "NELMDL", "value": "-6"},
        {"code": "EDIFF", "value": "1.0e-4"},
        {"code": "ISMEAR", "value": "2"},
        {"code": "SIGMA", "value": "0.2"},
        {"code": "VOSKOWN", "value": "true"},
        {"code": "ISPIN", "value": "2"},
        {"code": "LWAVE", "value": "false"},
        {"code": "LCHARG", "value": "true"},
        {"code": "NEDOS", "value": "2500"},
        {"code": "NSW", "value": "50"},
        {"code": "EDIFFG", "value": "1.0E-2"},
        {"code": "IBRION", "value": "1"},
        {"code": "ISIF", "value": "3"}
    ],
});