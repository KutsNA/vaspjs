import React from "react";
import {Form, Table} from "semantic-ui-react";
import PropTypes from "prop-types";

const EditableTable = (props) => {
    const {data, onChange, tableStyle, additionalElements} = props;

    return (
        <Table {...tableStyle}>
            <Table.Body>
                <Table.Row>
                    {
                        data.map((dataRow, rowIndex) =>
                            Array.isArray(dataRow) ?
                                createRow(dataRow, rowIndex, onChange, additionalElements) :
                                createCellElement(0, dataRow, rowIndex, onChange, additionalElements)
                        )
                    }
                </Table.Row>
            </Table.Body>
        </Table>
    );
};

const createRow = (dataRow, rowIndex, onChange, additionalElements) =>
    <Table.Row>
        {
            dataRow.map((dataEl, elIndex) =>
                createCellElement(rowIndex, dataEl, elIndex, onChange, additionalElements)
            )
        }
    </Table.Row>;

const createCellElement = (rowIndex, dataEl, elIndex, onChange, additionalElements) =>{
    console.log(additionalElements)
 return   <Table.Cell style={{padding: '5px'}}>
     <Form inline>
        <Form.Input name={{rowIndex, elIndex}} value={dataEl}
                    onChange={onChange}/>
                    {additionalElements()}
                    </Form>
    </Table.Cell>;
}


export default EditableTable;


EditableTable.propTypes = {
    data: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
};

EditableTable.defaultProps = {
    tableStyle: {},
    additionalElements: () => {}
};