import React from "react";
import ReactDOM from "react-dom";
import {AppContainer, hot} from "react-hot-loader";

import App from "./App";

// const render = () => {
//     ReactDOM.render(
//         <AppContainer>
//             <App/>
//         </AppContainer>,
//         document.getElementById("root")
//     );
// };
//
// export default hot(render());
//
// if (module.hot) {
//     module.hot.accept('./index.js', () => render());
// }
ReactDOM.render(<App/>, document.getElementById("root"));