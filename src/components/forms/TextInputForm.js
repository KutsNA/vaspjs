import React from "react";
import PropTypes from "prop-types";

import FormComponent from './FormComponent'

const TextInputForm = (props) => {
    const {label, handleChange, parameterName, parameterValue} = props;

    return (
        <FormComponent handleChange={handleChange} label={label} parameterValue={parameterValue} parameterName={parameterName}/>
    );
};

export default TextInputForm;

TextInputForm.propTypes = {
    label: PropTypes.string.isRequired,
    parameterName: PropTypes.string.isRequired,
    parameterValue: PropTypes.any.isRequired,
    handleChange: PropTypes.func.isRequired
};
