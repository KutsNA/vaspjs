import React from "react";
import {Form} from "semantic-ui-react";
import PropTypes from "prop-types";


const FormComponent = (props) => {
    const {label, handleChange, parameterName, parameterValue} = props;

    return (
        <Form.Input label={label} name={parameterName} value={parameterValue} onChange={handleChange}/>
    );
};

export default FormComponent

FormComponent.propTypes = {
    label: PropTypes.string.isRequired,
    parameterName: PropTypes.string.isRequired,
    parameterValue: PropTypes.any.isRequired,
    handleChange: PropTypes.func.isRequired
};
