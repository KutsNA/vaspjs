import React, {memo} from 'react';
import PropTypes from 'prop-types';
import {Checkbox, Form, Label, Select, Table} from 'semantic-ui-react';

import NumberInputForm from '../forms/NumberInputForm'
import TextInputForm from '../forms/TextInputForm'

const scientificNumbersPattern = /^\d*\.\d*[eE]-(\d)+$/;
const floatNumbersPattern = /^-?\d*(\.\d+)?$/;

const inputElement = (parameter, handler) => {
    switch (parameter.type) {
        case ('string'):
            return <TextInputForm label={''} parameterName={parameter.code} parameterValue={parameter.defaultValue}
                                  handleChange={handler}/>;
        case ('enum'):
            const options = parameter.options.map(option => {
                return {key: option.code, value: option.code, text: option.displayName}
            });
            return <Select options={options} name={parameter.code} defaultValue={options[0].key} onChange={handler}/>;
        case ('bool'):
            return <Checkbox label={parameter.code}/>;
        case ('int'):
            return <NumberInputForm label={''} parameterName={parameter.code}
                                    parameterValue={parseFloat(parameter.defaultValue)}
                                    handleChange={handler}/>;
        default:
            return <TextInputForm label={''} parameterName={parameter.code} parameterValue={parameter.defaultValue}
                                  handleChange={handler}/>;
    }
};
//TODO validation for scientific numbers
const handleFloatValueInput = handler => event => {
    const {name, value} = event.target;
    if (scientificNumbersPattern.test(value) || floatNumbersPattern.test(value)) {
        handler(event, {name, value});
    }
};

const ParameterPanel = (props) => {
    const {parameters, handleParameterPanelChange} = props;
    const parametersList = parameters.slice();
    return (
        <React.Fragment>
            <Label ribbon color='red'>Параметры расчета</Label>
            <div style={{maxHeight: '500px', overflowX: 'auto'}}>
                <Table striped>
                    <Table.Body>
                        {parametersList.map(parameter =>
                            <Table.Row>
                                <Table.Cell>
                                    {parameter.code}
                                </Table.Cell>
                                <Table.Cell>
                                    {Boolean(parameter.docUrl) ? <a href={parameter.docUrl}>{parameter.name}</a> :
                                        <p>{parameter.name}</p>}
                                </Table.Cell>
                                <Table.Cell>
                                    {
                                        inputElement(parameter, handleParameterPanelChange)
                                    }
                                </Table.Cell>
                            </Table.Row>
                        )}
                    </Table.Body>
                </Table>
            </div>
        </React.Fragment>
    );
};

export default memo(ParameterPanel);


ParameterPanel.propTypes = {
    system: PropTypes.string,
    prec: PropTypes.string,
    nWrite: PropTypes.number,
    nelm: PropTypes.number,
    nelmIn: PropTypes.number,
    nelmDl: PropTypes.number,
    ediff: PropTypes.number,
    ismear: PropTypes.number,
    sigma: PropTypes.number,
    ispin: PropTypes.number,
    nedos: PropTypes.number,
    nsw: PropTypes.number,
    ediffg: PropTypes.number,
    ibrion: PropTypes.number,
    isif: PropTypes.number,
    lwave: PropTypes.bool,
    lcharg: PropTypes.bool,
    voskown: PropTypes.bool,
    handleParameterPanelChange: PropTypes.func.isRequired
};