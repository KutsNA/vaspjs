import axios from 'axios';

//const root = 'http://crystal.pmoproject.ru';
const root = 'http://localhost:8080';

export async function getChemicalElementsList() {
  const options = {
    method: 'GET',
    url: `${root}/api/dictionary/ChemicalElement`,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
  const {data} = await axios(options);
  return data;
};

export async function getDefaultConfig() {
  const options = {
    method: 'GET',
    url: `${root}/vasp/config`,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
  const {data} = await axios(options);
  return data;
};

export async function getUserListOfActiveTask() {
  const options = {
    method: 'GET',
    url: `${root}/vasp/run`,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
  return axios(options);
};

function createRequest(requestBodyInfo) {
  console.log(requestBodyInfo);
  const {atoms, kPoints, selectedProfile, parameters, structure} = requestBodyInfo;
  const incar = convertParameters(parameters);
  const request = {
    runProfileName: selectedProfile.profileName,
    input: {
      incar,// "incar": [ {"code": "SYSTEM", "value": "System name" }, {...}]
      atoms, // "atoms": { "atomTypes": [ {"chemicalElementSymbol": "Al","atomCount": 4,"potentialArchiveFile": "potcar_paw_gga.tar","potentialAtomFolder": "Al"}],"coordinateType": "Direct", "selectiveDynamics": true, "atomCoords": [{"no": 1,"chemicalElementSymbol": "Al","x1": 0.0,"x2": 0.0,"x3": 0.0,"sd1": true,"sd2": true,"sd3": true},
      structure, // "structure": {"latticeConstant": 1.0, "latticeVectors": [ [3.65,0.0,0.0],[0.0,3.65,0.0],[0.0,0.0,3.65]]
      kpoints: kPoints // "kpoints": {"meshGenerationType": "Gamma","length": 0,"divisions": [11,11,11],"shifts": [0.0,0.0,0.0],"basisVectors": [[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]]
    }
  };
  console.log(request);
  return request;
};

function convertParameters(parameters) {
  return parameters.map(parameter => {
    return {code: parameter.code, value: parameter.defaultValue};
  });
};

export async function initialCalculation(calculationParameters) {
  const params = createRequest(calculationParameters);
  const options = {
    method: 'POST',
    url: `${root}/vasp/run`,
    header: {'content-type': 'application/json'},
    data: params,
  };
  return axios(options);
  // Example output:
  // {
  //   "id": "bd80f6ae-2c18-478f-b67b-6b241dd79227",
  //   "userId": "SYSTEM",
  //   "startTime": 1557085700.083000000,
  //   "endTime": null,
  //   "status": "STARTED"
  // }
};

export async function getCalculationInfo(runId) {
  const options = {
    method: 'GET',
    url: `${root}/vasp/run/${runId}`
  };
  return axios(options);
}

export async function getCalculationLogs(runId/* = 'f47e5c81-8f4c-4c54-935a-ea44278a27fd'*/) {
  const options = {
    method: 'GET',
    url: `${root}/vasp/run/${runId}/log`
  };
  return axios(options);
};

export async function stopCalculation(runId) {
  const options = {
    method: 'DELETE',
    url: `${root}/vasp/run/${runId}`
  };
  axios(options).then(response => response.status === 200 ? console.log('Job stopped') : console.error(response));
};

export async function getResult(runId) {
  const options = {
    method: 'GET',
    url: `${root}/vasp/run/${runId}/result`
  };
  return axios(options);
};
